﻿//set variabel map
const myMap = L.map('map');
//base map inladen van "LifLet map"
const myBasemap = L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    maxZoom: 19,
    attribution: '© <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
});

//basemap toevogen aan myMap
myBasemap.addTo(myMap);
//map vie op Antwerpen instelen
myMap.setView([51.2194475, 4.4024643], 12);
//inlezen van fitnes objecten
let request = new XMLHttpRequest();
request.open('get', '/data/fitnessen.json', true);
request.onload = function () {
    const fitnessenList = JSON.parse(this.response);
    //alert(this.response);
    fitnessenList.map(fitnes => {
        console.log(fitnes.name);

        L.marker([fitnes.latitude, fitnes.longitude]).bindPopup(`
            <h2>${fitnes.name}</h2>
            <p><b>Fitnes:</b><br>
            ${fitnes.name}<br>
            ${fitnes.street}<br>
            ${fitnes.postalcode}</p>
            <p><b>Tel: </b>${fitnes.phone}</p>
        `).openPopup().addTo(myMap);
    });
    //met reduct kan ik bepalen hoevel enkele postkodes heb ik en optelen hoevel zijn er
    const postcodeCount = fitnessenList.reduce((sums, fitnes) => {
        sums[fitnes.postalcode] = (sums[fitnes.postalcode] || 0) + 1;
        return sums;
    }, {});

    //nu siedbar aanmaken (Lijst)
    const sidebar = document.getElementById('fitPerPostcode');
    const h3 = document.createElement('h3');
    h3.innerHTML = "Gementen Count";
    sidebar.appendChild(h3);

    //elementen toevoegen naar de siedbaar
    for (let postalcode in postcodeCount) {
        const p = document.createElement('p');
        p.innerHTML = `<b>${postalcode}</b> :<br>${postcodeCount[postalcode]}`;
        sidebar.appendChild(p);
    }
};
request.send();
